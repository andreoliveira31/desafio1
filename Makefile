all: km.o
	gcc -o km km.o -lm -pg
	
km.o:
	gcc -c km.c -pg
	
clean:
	rm km *.o
